var position = $(window).scrollTop(); 

$(function() {
    localStorage.clear();
    let videos = [];
    $('video').each(function() {
        videos.push($(this).attr('id'));
    })
    localStorage.setItem('videos', JSON.stringify(videos));
})

$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if(scroll > position) {
        let videoPlaying = localStorage.getItem('videoPlaying');
        if (videoPlaying) {
            if (!$('#' + localStorage.getItem('videoPlaying')).inView()) {
                let videos    = JSON.parse(localStorage.getItem('videos'));
                let nextVideo = parseInt(videos.indexOf(videoPlaying)) + 1;
                let videoId   = videos[nextVideo].substring(videos[nextVideo].lastIndexOf('-') + 1);
                playPause($('#' + videos[nextVideo]), videoId, 'scroll');
            }
        }
    } else {
        if (localStorage.getItem('videoPlaying')) {
            if ($('#' + localStorage.getItem('videoPlaying')).inViewUp()) {
                let stringVideo = localStorage.getItem('videoPlaying').substring(localStorage.getItem('videoPlaying').lastIndexOf('-') + 1);
                let videoSelector = $('#' + localStorage.getItem('videoPlaying'));
                if (videoSelector.offset().top > 400) {
                    videoSelector[0].pause();
                    $('#play-video-icon-' + stringVideo).show();
                }

                // let videoPlayNow = $('#' + localStorage.getItem('videoPlaying'));
                // this.console.log(videoPlayNow);
                // this.console.log(playVideoIcon);
                // $('#play-video-icon-' + playVideoIcon).show()
                
                // let videos = JSON.parse(localStorage.getItem('videos'));
                // let videoPaused = localStorage.getItem('videoPlaying');
                // let preVideo = parseInt(videos.indexOf(videoPaused)) - 1;
                // if (videos[preVideo]) {
                    // if (!localStorage.getItem('currentVideoUp') || localStorage.getItem('currentVideoUp') != videos[preVideo]) {
                        // localStorage.setItem('currentVideoUp', videos[preVideo]);
                        // let playVideoIcon = videos[preVideo].substring(videos[preVideo].lastIndexOf('-') + 1);
                        // if (!$(`#video-item-${playVideoIcon} source`).attr('src')) {
                            // let videoUrl = $(`#video-item-${playVideoIcon}`).attr('url');
                            // $(`#video-item-${playVideoIcon} source`).attr('src', videoUrl);
                            // $(`#video-item-${playVideoIcon}`)[0].load();
                        // }
                    //     $(`#video-item-${playVideoIcon}`)[0].play();
                    //     $(`#video-item-${playVideoIcon}`)[0].onplay = function () {
                    //         $(`#play-video-icon-${playVideoIcon}`).hide();
                    //         $(`span#sound-${playVideoIcon}`).removeClass('sound-on');
                    //         $(`span#sound-${playVideoIcon}`).addClass('sound-off').show();
                    //         $(`#video-item-${playVideoIcon}`)[0].muted = true;
                    //         localStorage.setItem('videoPlaying', videos[preVideo]);
                    //     }
                    // }
                // }
            }
        }
    }
    
    position = scroll;

});

function playPause(e, id, scroll = null) {
    let videoUrl = $(`#video-item-${id}`).attr('url');
    $('.play-video-icon').show();
    $('video').each(function() {
        this.pause();
    })

    if (!$(`#video-item-${id} source`).attr('src')) {
        $(`#video-item-${id} source`).attr('src', videoUrl);
        $(`#video-item-${id}`)[0].load();
    }

    $(`#video-item-${id}`)[0].play();

    $(`#video-item-${id}`)[0].onplay = function () {
        $(`#play-video-icon-${id}`).hide();
        $(`span#sound-${id}`).removeClass('sound-on');
        $(`span#sound-${id}`).addClass('sound-off').show();
        $(`#video-item-${id}`)[0].muted = true;
    }

    // Play next video
    let videos       = JSON.parse(localStorage.getItem('videos'));
    let videoPlaying = `video-item-${id}`;
    if (videos.length > 0 && videoPlaying) {
        let nextVideo = parseInt(videos.indexOf(videoPlaying)) + 1;
        if (nextVideo < videos.length) {
            let nextVideoId = videos[nextVideo].substring(videos[nextVideo].lastIndexOf('-') + 1);

            if (!$(`#video-item-${nextVideoId} source`).attr('src')) {
                let nextVideoUrl = $(`#video-item-${nextVideoId}`).attr('url');
                $(`#video-item-${nextVideoId} source`).attr('src', nextVideoUrl);
                $(`#video-item-${nextVideoId}`)[0].load();
                $(`#video-item-${nextVideoId}`)[0].play();
                localStorage.setItem('videoLoaded', `video-item-${nextVideoId}`);

                $(`#video-item-${nextVideoId}`)[0].onplay = function () {
                    setTimeout(() => {
                        if (nextVideo != (videos.length - 1)) {
                            $('#' + localStorage.getItem('videoLoaded'))[0].pause();
                        }
                    }, 1000);
                };
            }
        }
    }

    localStorage.setItem('videoPlaying', `video-item-${id}`);
}

function pauseVideo(e) {
    e.pause();
    $('.play-video-icon').show();
}

function muteVideo(e, id) {
    var currentVideo = document.getElementById(`video-item-${id}`);
    if (currentVideo.muted) {
        currentVideo.muted = false;
        e.classList.remove("sound-off");
        e.classList.add('sound-on');
    } else {
        currentVideo.muted = true;
        e.classList.remove("sound-on"); 
        e.classList.add('sound-off');
    }
}

$(function() {
    $('.comment-click').click(function() {
        var videoId = $(this).attr('comment-video');
        if (!$(`.facebok-comment-id-${videoId}`).html()) {
            $.ajax({
                url : `/comment/${videoId}`,
                type : 'GET',
                success : function(res) {
                    $(`.facebok-comment-id-${videoId}`).html(res).show();
                    FB.XFBML.parse();
                },
                error : function(req, err) {
                }
            });
        } else {
            $(`.facebok-comment-id-${videoId}`).toggle();
        }
    });

    $('.more-less').click(function() {
        $(this).parent().css('height', 'auto');
    })

    $('.vote-up-event').click(function() {
        let self = $(this);
        let videoId = $(this).attr('video-id');
        let voteUp = parseInt($(this).find('.number').text()) + 1;
        $.ajax({
            url: `/upvote/${videoId}?type=upvote`,
            type: 'GET',
            success: function(res) {
                if (res.status == true) {
                    self.find('.number').text(voteUp);
                }
            }
        })
    })

    $('.vote-down-event').click(function() {
        let self = $(this);
        let videoId = $(this).attr('video-id');
        let voteDown = parseInt($(this).find('.number').text()) + 1;
        $.ajax({
            url: `/upvote/${videoId}?type=downvote`,
            type: 'GET',
            success: function(res) {
                if (res.status == true) {
                    self.find('.number').text(voteDown);
                }
            }
        })
    })

    $('i.play-gif').click(function() {
        var gifId = $(this).attr('gif-id');
        var gifImage = $(`#gif-image-${gifId}`).attr('data-alt');
        var gifSrc = $(`#gif-image-${gifId}`).attr('src');
        $(`#gif-image-${gifId}`).attr('src', gifImage);
        $(`#gif-image-${gifId}`).addClass('playing');
        $(`#gif-image-${gifId}`).attr('data-alt', gifSrc);
        $(this).hide();
    });

    $('img.gif-image-pause').click(function() {
        if ($(this).hasClass('playing')) {
            var gifImage = $(this).attr('src');
            var gifAlt = $(this).attr('data-alt');
            $(this).attr('src', gifAlt);
            $(this).attr('data-alt', gifImage);
            $(this).parent().find('.play-gif').show();
            $(this).removeClass('playing')
        }
    })

    $('span.less-more-btn').click(function() {
        let classVideoId = $(this).attr('lessmore');
        if ($(this).html() == 'Thu nhỏ') {
            $(this).html('Xem thêm');
            $(`.less-more-${classVideoId}`).css('height', '50px');
        } else {
            $(this).html('Thu nhỏ');
            $(`.less-more-${classVideoId}`).css('height', 'auto');
        }
    })
    
    // $('img.watermark').watermark({
    //     path: './img/babaimage-play-button-icon-png-displaying-18-images-for-red-play-button-icon-png-300x300.png',
    //     text: '',
    //     textWidth: 130,
    //     textSize: 13,
    //     textColor: 'white',
    //     textBg: 'rgba(0, 0, 0, 0.4)',
    //     gravity: 'se', 
    //     opacity: 1,
    //     margin: 250,
    //     outputWidth: 'auto',
    //     outputHeight: 'auto',
    //     outputType: 'jpeg',
    //     done: function (imgURL) {
    //         this.src = imgURL;
    //         console.log(imgURL);
    //     },
    //     fail: function (imgURL) {
    //     },

    //     always: function (imgURL) {
    //     }
    // });
})

$.fn.inView = function(){
    var win = $(window);
    obj = $(this);
    var scrollPosition = win.scrollTop();
    var visibleArea = win.scrollTop() + win.height();
    var objEndPos = (obj.offset().top + obj.outerHeight());
    return(visibleArea >= objEndPos && scrollPosition <= objEndPos ? true : false)
};

$.fn.inViewUp = function(){
    var win = $(window);
    var scrollBottom = $(document).height() - $(window).height() - $(window).scrollTop();
    obj = $(this);
    var scrollPosition = scrollBottom;
    var visibleArea = scrollBottom + win.height() ;
    var objEndPos = (obj.offset().top + obj.outerHeight() + 50);
    return(visibleArea >= objEndPos && scrollPosition <= objEndPos ? true : false)
};
