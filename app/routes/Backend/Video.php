<?php

Route::group(['prefix' => 'video', 'as' => 'video.', 'namespace' => 'Video'], function () {
    Route::get('/', 'VideoController@index')->name('index');
});
