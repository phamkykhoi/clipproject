<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use Illuminate\Http\Request;

/**
 * Class FrontendController.
 */
class FrontendController extends Controller
{
    protected $page;
    protected $video;

    public function __construct(PagesRepository $page)
    {
        $this->video = $page;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        try {
            $videos = $this->video->getAllVideo($request->get('filter'));
            $categories = $this->video->getCategories();
            $setting = Setting::first();
            return view('frontend.index')->with([
                'videos' => $videos,
                'categories' => $categories,
                'setting' => $setting
            ]);
        } catch(\Exception $e) {
            dd();
        }
    }

    public function showPage($slug, PagesRepository $pages)
    {
        $page = explode('-', $slug);
        $categories = $this->video->getCategories();
        $setting = Setting::first();

        switch (end($page)) {
            case 'blog':
                $video = $this->video->findVideoBySlug($slug);
            
                if (!$video) {
                    dd('Video không tồn tại');
                }

                $video->view_count += 1;
                $video->save();
                $videoRelations = $this->video->getVideoRelation($video);
                
                return view('frontend.pages.index')->with([
                    'video' => $video,
                    'videoRelations' => $videoRelations,
                    'categories' => $categories,
                    'setting' => $setting
                ]);
                break;

            case 'tag':
                $page = $this->video->getVideoByTag($slug);
                return view('frontend.tags.index')->with([
                    'videos' => $page['videos'],
                    'tag' => $page['tag'],
                    'categories' => $categories,
                    'setting' => $setting
                ]);
                break;

            case 'cate':
                $page = $this->video->getVideoByCategories($slug);
                return view('frontend.categories.index')->with([
                    'videos' => $page['videos'],
                    'category' => $page['category'],
                    'categories' => $categories,
                    'setting' => $setting
                ]);
                break;
        }
    }

    public function vote(Request $request, $videoId)
    {
        return response()->json([
            'status' => $this->video->vote($videoId, $request->get('type'))
        ]);
    }
}
