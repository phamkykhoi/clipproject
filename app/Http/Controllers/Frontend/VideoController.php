<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blogs\Blog;
use Session;

class VideoController extends Controller
{
    public function index()
    {
        return view('frontend.videos.ajax');
    }

    public function show($id)
    {
        $video = Blog::find($id);
        return view('frontend.videos.show')->with([
            'video' => $video
        ]);
    }

    public function deleteAllVideo(Request $request)
    {
        try {
            $ids = $request->id;
            if ($request->id != null) {
                Blog::destroy($ids);
                Session::flash('flash_success', 'Xóa thành công'); 
                return response()->json(['status' => true]);
            } else {
                Session::flash('flash_warning', 'Vui lòng chọn cần xóa video'); 
                return response()->json(['status' => true]);
            }
        } catch(Exception $e) {
            Session::flash('flash_danger', 'Xóa thất bại'); 
            return response()->json(['status' => false]);
        }
    }
}
