<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blogs\Blog;

class CommentController extends Controller
{
    public function index($id)
    {
        $video = Blog::find($id);
        return view('frontend.comments.index')->with([
            'video' => $video
        ]);
    }
}
