<?php

namespace App\Http\Requests\Backend\Blogs;

use App\Http\Requests\Request;

/**
 * Class StoreBlogsRequest.
 */
class StoreBlogsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('create-blog');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required|max:191',
            'publish_datetime'  => 'required|date',
            // 'categories'        => 'required',
            // 'tags'              => 'required',
            'status'            => 'required',
        ];
    }

    /**
     * Get the validation message that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Vui lòng đặt tên nội dung',
            // 'categories.required' => 'Vui lòng chọn danh mục',
            // 'tags.required' => 'Vui lòng chọn hoặc thêm tag',
        ];
    }
}
