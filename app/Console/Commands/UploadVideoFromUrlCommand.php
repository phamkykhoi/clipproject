<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Log;

class UploadVideoFromUrlCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bath:upload {url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload video from url';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $sourceFile = $this->argument('url');
            if ($sourceFile) {
                $contents = file_get_contents($sourceFile);
                $name = time() . ".mp4";
                Storage::put('videos'.DIRECTORY_SEPARATOR .$name, $contents);
                $this->info(json_encode([
                    'status' => true,
                    'name' => $name
                ]));
            } else {
                $this->info(json_encode([
                    'status' => false,
                    'name' => null
                ]));
            }
        } catch(Exception $e) {
            $this->info(json_encode([
                'status' => false,
                'name' => null
            ]));
        }
    }
}
