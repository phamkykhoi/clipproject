<?php

namespace App\Repositories\Backend\Blogs;

use App\Events\Backend\Blogs\BlogCreated;
use App\Events\Backend\Blogs\BlogDeleted;
use App\Events\Backend\Blogs\BlogUpdated;
use App\Exceptions\GeneralException;
use App\Models\BlogCategories\BlogCategory;
use App\Models\BlogMapCategories\BlogMapCategory;
use App\Models\BlogMapTags\BlogMapTag;
use App\Models\Blogs\Blog;
use App\Models\BlogTags\BlogTag;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Storage;
use Artisan;

/**
 * Class BlogsRepository.
 */
class BlogsRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Blog::class;

    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct()
    {
        $this->upload_path = 'videos'.DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->leftjoin(config('access.users_table'), config('access.users_table').'.id', '=', config('module.blogs.table').'.created_by')
            ->orderBy('blogs.id', 'DESC')
            ->select([
                config('module.blogs.table').'.id',
                config('module.blogs.table').'.slug',
                config('module.blogs.table').'.upvote',
                config('module.blogs.table').'.downvote',
                config('module.blogs.table').'.view_count',
                config('module.blogs.table').'.name',
                config('module.blogs.table').'.publish_datetime',
                config('module.blogs.table').'.status',
                config('module.blogs.table').'.created_by',
                config('module.blogs.table').'.created_at',
                config('access.users_table').'.first_name as user_name',
            ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {
        $fileInfo = [];
        if ($input['linkvideo']) {
            $command = 'bath:upload';
            $params = ['url' => $input['linkvideo']]; // https://i.imgur.com/DutKgkx.mp4;
            Artisan::call($command, $params);
            $fileInfo = json_decode(Artisan::output(), true);
        }

        $tagsArray = $this->createTags($input['tags'] ?? []);
        $categoriesArray = $this->createCategories($input['categories'] ?? []);
        if (isset($input['categories'])) {
            unset($input['tags']);    
        }

        if (isset($input['categories'])) {
            unset($input['categories']);    
        }

        DB::transaction(function () use ($input, $tagsArray, $categoriesArray, $fileInfo) {
            $input['slug'] = convertToSlug($input['name'], 'blog');
            $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);

            if (isset($input['featured_image'])) {
                $thumbs = $this->uploadImage($input);
                $input['featured_image'] = isset($thumbs['featured_image']) && $thumbs['featured_image'] != null ? $thumbs['featured_image'] : null;
            }

            if (isset($input['sharing_image'])) {
                $thumbs = $this->uploadImageSharing($input);
                $input['sharing_image'] = isset($thumbs['sharing_image']) && $thumbs['sharing_image'] != null ? $thumbs['sharing_image'] : null;
            }

            if ($fileInfo != null && $fileInfo['status']) {
                $input['video'] = $fileInfo['name'];
            } elseif (isset($input['video'])) {
                $videoFile = $this->uploadVideo($input);
                $input['video'] = isset($videoFile['video']) && $videoFile['video'] != null ? $videoFile['video'] : null;
            }

            $input['created_by'] = access()->user()->id;
            $input = array_filter($input);

            if ($blog = Blog::create($input)) {
                // Inserting associated category's id in mapper table
                if (count($categoriesArray)) {
                    $blog->categories()->sync($categoriesArray);
                }

                // Inserting associated tag's id in mapper table
                if (count($tagsArray)) {
                    $blog->tags()->sync($tagsArray);
                }

                event(new BlogCreated($blog));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.blogs.create_error'));
        });
    }

    /**
     * Update Blog.
     *
     * @param \App\Models\Blogs\Blog $blog
     * @param array                  $input
     */
    public function update(Blog $blog, array $input)
    {
        $fileInfo = [];
        if ($input['linkvideo'] && trim($blog->linkvideo) != trim($input['linkvideo'])) {
            $command = 'bath:upload';
            $params = ['url' => $input['linkvideo']];
            Artisan::call($command, $params);
            $fileInfo = json_decode(Artisan::output(), true);
        }


        $tagsArray = $this->createTags($input['tags'] ?? []);
        $categoriesArray = $this->createCategories($input['categories'] ?? []);
        if (isset($input['categories'])) {
            unset($input['tags']);    
        }

        if (isset($input['categories'])) {
            unset($input['categories']);    
        }

        $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
        $input['updated_by'] = access()->user()->id;

        if (trim($blog->name) == trim($input['name'])) {
            $input['slug'] = convertToSlug($input['name'], 'blog');
        }

        if (isset($input['featured_image'])) {
            $this->deleteOldFile($blog, 'featured_image');
            $thumbs = $this->uploadImage($input);
            $input['featured_image'] = isset($thumbs['featured_image']) && $thumbs['featured_image'] != null ? $thumbs['featured_image'] : null;
        }

        if (isset($input['sharing_image'])) {
            $thumbs = $this->uploadImageSharing($input);
            $input['sharing_image'] = isset($thumbs['sharing_image']) && $thumbs['sharing_image'] != null ? $thumbs['sharing_image'] : null;
        }

        if ($fileInfo != null && $fileInfo['status']) {
            $input['video'] = $fileInfo['name'];
        } else if (isset($input['video'])) {
            $this->deleteOldFile($blog, 'video');
            $videoFile = $this->uploadVideo($input);
            $input['video'] = isset($videoFile['video']) && $videoFile['video'] != null ? $videoFile['video'] : null;
        }

        DB::transaction(function () use ($blog, $input, $tagsArray, $categoriesArray) {

            if (!$input['featured_image']) {
                unset($input['featured_image']);
            }

            if ($blog->update($input)) {

                // Updateing associated category's id in mapper table
                if (count($categoriesArray)) {
                    $blog->categories()->sync($categoriesArray);
                }

                // Updating associated tag's id in mapper table
                if (count($tagsArray)) {
                    $blog->tags()->sync($tagsArray);
                }

                event(new BlogUpdated($blog));

                return true;
            }

            throw new GeneralException(
                trans('exceptions.backend.blogs.update_error')
            );
        });
    }

    /**
     * Creating Tags.
     *
     * @param array $tags
     *
     * @return array
     */
    public function createTags($tags)
    {
        //Creating a new array for tags (newly created)
        $tags_array = [];

        foreach ($tags as $tag) {
            if (is_numeric($tag)) {
                $tags_array[] = $tag;
            } else {
                $newTag = BlogTag::create([
                    'name' => $tag,
                    'slug' => convertToSlug($tag, 'tag'),
                    'status' => 1,
                    'created_by' => 1
                ]);
                $tags_array[] = $newTag->id;
            }
        }

        return $tags_array;
    }

    /**
     * Creating Categories.
     *
     * @param Array($categories)
     *
     * @return array
     */
    public function createCategories($categories)
    {
        //Creating a new array for categories (newly created)
        $categories_array = [];

        foreach ($categories as $category) {
            if (is_numeric($category)) {
                $categories_array[] = $category;
            } else {
                $newCategory = BlogCategory::create([
                    'name' => $category,
                    'slug' => convertToSlug($category, 'cate'),
                    'status' => 1,
                    'created_by' => 1
                ]);

                $categories_array[] = $newCategory->id;
            }
        }

        return $categories_array;
    }

    /**
     * @param \App\Models\Blogs\Blog $blog
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Blog $blog)
    {
        DB::transaction(function () use ($blog) {
            if ($blog->delete()) {
                BlogMapCategory::where('blog_id', $blog->id)->delete();
                BlogMapTag::where('blog_id', $blog->id)->delete();

                event(new BlogDeleted($blog));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.blogs.delete_error'));
        });
    }

    /**
     * Upload Image.
     *
     * @param array $input
     *
     * @return array $input
     */
    public function uploadVideo($input)
    {
        ini_set('post_max_size', '100M');
        ini_set('upload_max_filesize', '100M');
        ini_set('memory_limit', '32M');
        ini_set('max_execution_time', 0);
        $video = $input['video'];

        if (isset($input['video']) && !empty($input['video'])) {
            $currentTime = time();
            $fileName = $currentTime . $video->getClientOriginalName();
            $fileType = $video->getClientOriginalExtension();
            
            $this->storage->put($this->upload_path . $fileName, file_get_contents($video->getRealPath()));

            if ($fileType == 'gif') {
                $imageConverted = imagepng(imagecreatefromstring(file_get_contents(env('APP_URL') . '/files/videos/' . $fileName)), "./files/background/${currentTime}.png");
            }

            return $input = array_merge($input, [
                'video' => $fileName,
                'filetype' => $fileType,
                'featured_image' => !empty($imageConverted) ? "${currentTime}.png" : null
            ]);
        }
    }

    public function uploadImage($input)
    {
        ini_set('post_max_size', '100M');
        ini_set('upload_max_filesize', '100M');

        $image = $input['featured_image'];
        $stringImage = substr($image, 0, strpos($image, ';'));
        $extend = substr($stringImage, strpos($stringImage, '/') + 1, strlen($stringImage));

        if (isset($input['featured_image']) && !empty($input['featured_image'])) {
            $image = str_replace("$stringImage;base64,", '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . ".$extend";
            $this->storage->put('background/' . $imageName, base64_decode($image));
            return array_merge($input, ['featured_image' => $imageName]);
        }
    }

    public function uploadImageSharing($input)
    {
        ini_set('post_max_size', '100M');
        ini_set('upload_max_filesize', '100M');

        $image = $input['sharing_image'];
        $stringImage = substr($image, 0, strpos($image, ';'));
        $extend = substr($stringImage, strpos($stringImage, '/') + 1, strlen($stringImage));

        if (isset($input['sharing_image']) && !empty($input['sharing_image'])) {
            $image = str_replace("$stringImage;base64,", '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(20) . ".$extend";
            $this->storage->put('background/' . $imageName, base64_decode($image));
            return array_merge($input, ['sharing_image' => $imageName]);
        }
    }

    /**
     * Destroy Old Image.
     *
     * @param int $id
     */
    public function deleteOldFile($model, $key)
    {
        $fileName = $model->$key;

        return $this->storage->delete($this->upload_path . $fileName);
    }
}
