<?php

namespace App\Repositories\Frontend\Pages;

use App\Exceptions\GeneralException;
use App\Models\Page\Page;
use App\Repositories\BaseRepository;
use App\Models\Blogs\Blog;
use App\Models\BlogTags\BlogTag;
use App\Models\BlogCategories\BlogCategory;

/**
 * Class PagesRepository.
 */
class PagesRepository extends BaseRepository
{
    protected $columns = [
        'blogs.id',
        'blogs.name',
        'blogs.slug',
        'featured_image',
        'youtube',
        'blogs.video',
        'blogs.upvote',
        'blogs.downvote',
        'blogs.view_count',
        'blogs.filetype',
        'blogs.content',
        'blogs.sharing_image'
    ];

    /**
     * Associated Repository Model.
     */
    const MODEL = Page::class;

    /*
    * Find page by page_slug
    */
    public function findBySlug($page_slug)
    {
        if (!is_null($this->query()->wherePage_slug($page_slug)->firstOrFail())) {
            return $this->query()->wherePage_slug($page_slug)->firstOrFail();
        }

        throw new GeneralException(trans('exceptions.backend.access.pages.not_found'));
    }

    public function getAllVideo($filter = null)
    {
        $filter = trim($filter);
        $query = Blog::select($this->columns)->with(['tags' => function($query) {
            $query->select('name', 'slug');
        }]);
        
        $query->orderBy('blogs.id', 'DESC');

        if ($filter == 'new') {
            $query->orderBy('blogs.publish_datetime', 'DESC');
        } else if ($filter == 'viewest') {
            $query->orderBy('blogs.view_count', 'DESC');
        } else if($filter == 'random') {
            $query->orderBy('blogs.upvote', 'DESC');
        }
        
        return $query->where(['blogs.status' => 'Published'])->simplePaginate();
    }

    public function findVideoBySlug($slug)
    {
        $columns = array_merge($this->columns, ['content', 'meta_keywords']);
        return Blog::select($columns)->where('slug', $slug)->first();
    }

    public function getVideoByTag($slug)
    {
        $tag = BlogTag::where('slug', $slug)->first();
        return [
            'videos' => Blog::select($this->columns)
                ->join('blog_map_tags', 'blog_map_tags.blog_id', 'blogs.id')
                ->join('blog_tags', 'blog_map_tags.tag_id', 'blog_tags.id')
                ->where('blog_tags.id', $tag->id)
                ->orderBy('blogs.id', 'DESC')
                ->where(['blogs.status' => 'Published'])
                ->simplePaginate(),
            'tag' => $tag
        ];
    }

    public function getVideoByCategories($slug)
    {
        $category = BlogCategory::where('slug', $slug)->first();
        
        return [
            'videos' => Blog::select($this->columns)
                ->join('blog_map_categories', 'blog_map_categories.blog_id', 'blogs.id')
                ->join('blog_categories', 'blog_map_categories.category_id', 'blog_categories.id')
                ->where('blog_categories.id', $category->id)
                ->orderBy('blogs.id', 'DESC')
                ->where(['blogs.status' => 'Published'])
                ->simplePaginate(),
            'category' => $category
        ];
    }

    public function getVideoRelation($video)
    {
        $categoryIds = $video->categories->pluck('id')->toArray();
        $tagIds = $video->tags->pluck('id')->toArray();
        $query = Blog::select($this->columns)->where('blogs.id', '!=', $video->id);

        // if ($categoryIds != null) {
        //     $query->join('blog_map_categories', 'blog_map_categories.blog_id', 'blogs.id')
        //         ->join('blog_categories', 'blog_map_categories.category_id', 'blog_categories.id')
        //         ->whereIn('blog_categories.id', $categoryIds);
        // }

        if ($tagIds != null) {
            $query->join('blog_map_tags', 'blog_map_tags.blog_id', 'blogs.id')
                ->join('blog_tags', 'blog_map_tags.tag_id', 'blog_tags.id')
                ->whereIn('blog_tags.id', $tagIds);
        }
            
        return $query->distinct('blogs.id')
            ->orderBy('blogs.id', 'DESC')
            ->where(['blogs.status' => 'Published'])
            ->simplePaginate();
    }

    public function getCategories()
    {
        return BlogCategory::where('status', true)->get();
    }

    public function vote($videoId, $type)
    {
        $video = Blog::find($videoId);

        if ($type == 'upvote') {
            $video->upvote += 1;
        }
        
        if ($type == 'downvote') {
            $video->downvote += 1;
        }

        return (boolean) $video->save();
    }
}