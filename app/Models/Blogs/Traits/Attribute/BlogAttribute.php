<?php

namespace App\Models\Blogs\Traits\Attribute;

/**
 * Class BlogAttribute.
 */
trait BlogAttribute
{
    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">'.
            $this->getEditButtonAttribute('edit-blog', 'admin.blogs.edit').
            $this->getDeleteButtonAttribute('delete-blog', 'admin.blogs.destroy').
            '<a class="btn btn-default btn-flat" href="/'. $this->slug .'" target="__blank">
            <i data-toggle="tooltip" data-placement="top" title="Xem" class="fa fa-eye"></i></a></div>';
    }
}
