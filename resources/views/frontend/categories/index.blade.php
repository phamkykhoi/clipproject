@extends('frontend.layouts.app')

@section('title'){{ $category->name }}@endsection
@section('keywords'){{ $category->keywords }}@endsection
@section('description'){{ $category->description }}@endsection
@section('introduce'){{ $setting->footer_text }}@endsection

@section('content')

<section>
    <div class="container categories-block">
        <a href="#" class="tab">{{ $category->name }}</a>
    </div>
</section>

<div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                @include('frontend.videos.index', [
                    'videos' => $videos,
                    'categories' => $categories,
                ])
            </div>
        </div>
    </div>
@endsection
