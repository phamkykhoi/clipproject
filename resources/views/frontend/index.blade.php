@extends('frontend.layouts.app')

@section('title'){{ $setting->seo_title }}@endsection
@section('keywords'){{ $setting->seo_description }}@endsection
@section('description'){{ $setting->seo_keyword }}@endsection
@section('introduce'){{ $setting->footer_text }}@endsection

@section('content')

<section>
    <div class="container categories-block">
        <a href="{{ url('?filter=new') }}" class="tab">Mới nhất</a>
        <a href="{{ url('?filter=viewest') }}" class="tab">Xem nhiều</a>
        <a href="{{ url('?filter=random') }}" class="tab">Random</a>
    </div>
</section>

<div class="album py-5 bg-light">
            <div class="container">
                <div class="row">
                    @include('frontend.videos.index', [
                        'videos' => $videos,
                        'categories' => $categories,
                    ])
                </div>
            </div>
        </div>
@endsection
