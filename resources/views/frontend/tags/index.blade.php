@extends('frontend.layouts.app')

@section('title'){{ $tag->name }}@endsection
@section('keywords'){{ $tag->keywords }}@endsection
@section('description'){{ $tag->description }}@endsection
@section('introduce'){{ $setting->footer_text }}@endsection

@section('content')

<section>
    <div class="container categories-block">
        <a href="" class="tab">#{{ $tag->name }}</a>
    </div>
</section>

<div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                @include('frontend.videos.index', [
                    'videos' => $videos,
                    'categories' => $categories,
                ])
            </div>
        </div>
    </div>
@endsection
