@extends('frontend.layouts.app')
@section('title'){{ $video->name }}@endsection
@section('keywords'){{ $video->meta_keywords }}@endsection
@section('description'){{ $video->content }}@endsection
@section('introduce'){{ $setting->footer_text }}@endsection

@section('sharing')
    <meta property="og:type" content="movie" />
    <meta property="og:title" content="{{ $video->name }}" />
    <meta property="og:description" content="{{ $video->content }}" />
    <meta property="og:url" content="{{ url($video->slug) }}" />
    @if ($video->sharing_image)
        <meta property="og:image" content="{{ asset('files/background/' . $video->sharing_image) }}" />
    @else
        <meta property="og:image" content="{{ asset('files/background/' . $video->featured_image) }}" />
    @endif
    <meta property="og:video:type" content="application/x-shockwave-flash" />
    <meta property="og:video" content="{{ asset('files/videos/' . $video->video) }}" />
    <meta property="og:video:type" content="video/mp4" />
@endsection

@section('content')
    <section>
        <div class="container categories-block">
            <h3 class="detail-page-title">
                {{ $video->name }}
            </h3>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-8 video-grid">
                    @if ($video->filetype == 'gif')
                        @include('frontend.pages._gif', ['video' => $video])
                    @elseif ($video->filetype == 'webp')
                        @include('frontend.pages._webp', ['video' => $video])
                    @elseif ($video->youtube)
                        @include('frontend.pages._youtube', ['video' => $video])
                    @elseif (!$video->video && $video->featured_image)
                        @include('frontend.pages._photo', ['video' => $video])
                    @else                        
                        @include('frontend.pages._video', ['video' => $video])
                    @endif

                    @include('frontend.videos._list', ['videos' => $videoRelations])
                </div>
                
                @if (!empty($categories))
                    <div class="col-md-4 menu-right">
                        <h3>Danh mục</h3>
                        <ul>
                            @foreach ($categories as $category)
                            <li><a href="{{ url($category->slug) }}">{{ $category->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <script>
        window.onload = function() {
            var currentVideo = document.getElementById('video-item-{{ $video->id }}');
            document.getElementById('play-video-icon-{{ $video->id }}').style.display = 'none';
            document.getElementById('sound-{{ $video->id }}').style.display = 'block';
            currentVideo.play();
        };
    </script>
@endsection
