<div class="card mb-4 shadow-sm">
    <div class="video-info">
        <iframe width="100%" height="410" src="https://www.youtube.com/embed/{{ youtubeId($video->youtube) }}" frameborder="0" allowfullscreen></iframe>
    </div>
</div>