<div class="video-detail video-play" id="{{ $video->id }}">
    <div class="card mb-4 shadow-sm">
    <div class="video-info">
        <i class="fa fa-play play-video-icon"
        id="play-video-icon-{{ $video->id }}"
        aria-hidden="false"
        onclick="playPause(this, {{ $video->id }})"></i>
        <video class="video-item" id="video-item-{{ $video->id }}" loop="" playsinline="" onclick="pauseVideo(this)"
            poster="{{ asset('files/background/' . $video->featured_image) }}">
            <source src="" type="video/webm">
            <source src="{{ asset('files/videos/' . $video->video) }}" type="video/mp4">
        </video>

        <span class="sound sound-on"
            id="sound-{{ $video->id }}" 
            onclick="muteVideo(this, {{ $video->id }})"
            style='display: none;'>
        </span>
    </div>

    <div class="card-body">
        <div class="d-flex justify-content-between align-items-center">
            <div class="btn-group">
                <span class="upvote user-action vote-up-event" video-id="{{ $video->id }}">
                    <i class="icon-upvote"></i>
                    <span class="number">{{ $video->upvote }}</span>
                </span>

                <span class="upvote user-action vote-down-event" video-id="{{ $video->id }}">
                    <i class="icon-downvote"></i>
                    <span class="number">{{ $video->downvote }}</span>
                </span>

                <span class="upvote user-action view-icon">
                    <i class="fas fa-eye view-number"></i>{{ $video->view_count }}
                </span>

                <span class="sharing-group comment-click">
                <i class="fa fa-comments"></i>
                </span>

                <span class="sharing-group sharing-social" style="display: none;">
                    <i class="fa fa-share-alt"></i>
                </span>

                <div class="social-share-group">
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ url($video->slug) }}" target="_blank" title="Share on Facebook" onclick="javascript:window.open(this.href, '_blank', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false;">
                        <span class="sharing-group social-share">
                        <i class="fab fa-facebook-f"></i>
                        <span>Facebook</span></span>
                    </a>

                    <a href="https://twitter.com/intent/tweet?url={{ url($video->slug) }}" title="Share on Twitter" onclick="javascript:window.open(this.href, '_blank', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false;">
                        <span class="sharing-group social-share twitter"><i class="fab fa-twitter"></i><span>Twitter</span></span>
                    </a>

                    <a href="//www.pinterest.com/pin/create/button/?url={{ url($video->slug) }}&amp;media={{ asset('files/background/' . $video->featured_image) }}&amp;description={{ $video->name }}" title="Pin It" target="_blank" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false;">
                        <span class="sharing-group social-share pinterest"><i class="fab fa-pinterest-p"></i><span>Pinterest</span></span>
                    </a>
                </div>
            </div>
        </div>

        @if (!empty($video->content))
            <div class="video-description">
                {!! $video->content !!}
            </div>
        @endif

        @if(!empty($video->tags))
        <div class="tags">
            @foreach($video->tags as $tag)
                <a href="{{ url($tag->slug) }}" title="{{ $tag->name }}">#{{ $tag->name }}</a>
            @endforeach
        </div>
        @endif

        <div class="comment-block-item"></div>
    </div>
</div>
</div>