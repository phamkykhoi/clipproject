<div class="card mb-4 shadow-sm">
    <p class="card-text">
        <a href="{{ url($video->slug) }}"
           class="video-title"
           title="{{ $video->name }}">
            {{ $video->name }}
        </a>
    </p>

    <div class="video-info">
        <iframe width="100%" height="410" src="https://www.youtube.com/embed/{{ youtubeId($video->youtube) }}" frameborder="0" allowfullscreen></iframe>
    </div>
</div>