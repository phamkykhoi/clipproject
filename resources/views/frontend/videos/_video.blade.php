<div class="video-detail video-play" id="{{ $video->id }}">
    <div class="card mb-4 shadow-sm">
        <p class="card-text">
            <a href="{{ url($video->slug) }}" 
                class="video-title"
                title="{{ $video->name }}">
                {{ $video->name }}
            </a>
        </p>

        <div class="video-info">
            <i class="fa fa-play play-video-icon" id="play-video-icon-{{ $video->id }}" onclick="playPause(this, {{ $video->id }})"></i>
            <div class="video-{{ $video->id }} ajax-render">
                <video class="video-item"
                    id="video-item-{{ $video->id }}"
                    video-id="{{ $video->id }}"
                    loop=""
                    preload="metadata"
                    playsinline="" 
                    onclick="pauseVideo(this)"
                    url="{{ asset('files/videos/' . $video->video) }}"
                    poster="{{ asset('files/background/' . $video->featured_image) }}">
                    <source src="" type="video/mp4">
                </video>
            </div>

            <span class="sound sound-on" id="sound-{{ $video->id }}" 
                onclick="muteVideo(this, {{ $video->id }})" style='display: none;'></span>
        </div>
        <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                    <span class="upvote user-action vote-up-event" video-id="{{ $video->id }}">
                        <i class="icon-upvote"></i>
                        <span class="number">{{ $video->upvote }}</span>
                    </span>

                    <span class="upvote user-action vote-down-event" video-id="{{ $video->id }}">
                        <i class="icon-downvote"></i>
                        <span class="number">{{ $video->downvote }}</span>
                    </span>

                    <span class="upvote user-action view-icon">
                        <i class="fas fa-eye view-number"></i>{{ $video->view_count }}
                    </span>

                    <span class="sharing-group comment-click" comment-video="{{ $video->id }}">
                    <i class="fa fa-comments"></i>
                    </span>

                    <div class="social-share-group">
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ url($video->slug) }}" target="_blank" title="Share on Facebook" onclick="javascript:window.open(this.href, '_blank', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false;">
                            <span class="sharing-group social-share">
                            <i class="fab fa-facebook-f"></i>
                            <span>Facebook</span></span>
                        </a>

                        <a href="https://twitter.com/intent/tweet?url={{ url($video->slug) }}" title="Share on Twitter" onclick="javascript:window.open(this.href, '_blank', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false;">
                            <span class="sharing-group social-share twitter"><i class="fab fa-twitter"></i><span>Twitter</span></span>
                        </a>

                        <a href="//www.pinterest.com/pin/create/button/?url={{ url($video->slug) }}&amp;media={{ asset('files/background/' . $video->featured_image) }}&amp;description={{ $video->name }}" title="Pin It" target="_blank" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false;">
                            <span class="sharing-group social-share pinterest"><i class="fab fa-pinterest-p"></i><span>Pinterest</span></span>
                        </a>
                    </div>
                </div>
            </div>

            @if (trim($video->content) != null)
                <div class="video-description less-more less-more-{{ $video->id }}">
                    {!! $video->content !!}
                </div>
                @if (strlen($video->content) > 120)
                    <span class="less-more-btn" lessmore="{{ $video->id }}">Xem thêm</span>
                @endif
            @endif
            
            @if(!empty($video->tags))
            <div class="tags">
                @foreach($video->tags as $tag)
                    <a href="{{ url($tag->slug) }}" title="{{ $tag->name }}">#{{ $tag->name }}</a>
                @endforeach
            </div>
            @endif
            <div class="comment-block-item facebok-comment-id-{{ $video->id }}" style="display: none;"></div>
        </div>
    </div>
</div>