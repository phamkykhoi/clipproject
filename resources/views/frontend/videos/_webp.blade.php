<div class="video-detail" id="{{ $video->id }}">
    <div class="card mb-4 shadow-sm">
        <p class="card-text">
            <a href="{{ url($video->slug) }}" 
                class="video-title"
                title="{{ $video->name }}">
                {{ $video->name }}
            </a>
        </p>

        <div class="video-info" style="background: #000;">
            <i class="fa fa-play play-video-icon play-gif" gif-id="{{ $video->id }}" aria-hidden="false"></i>
            <img src="{{ asset('files/videos/' . $video->video) }}"
                id="gif-image-{{ $video->id }}" style="width: 100%; max-width: 500px"
                class="gif-image-pause" data-alt="{{ asset('files/videos/' . $video->video) }}" />
        </div>

        <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                    <span class="upvote user-action vote-up-event" video-id="{{ $video->id }}">
                        <i class="icon-upvote"></i>
                        <span class="number">{{ $video->upvote }}</span>
                    </span>

                    <span class="upvote user-action vote-down-event" video-id="{{ $video->id }}">
                        <i class="icon-downvote"></i>
                        <span class="number">{{ $video->downvote }}</span>
                    </span>

                    <span class="upvote user-action view-icon">
                        <i class="fas fa-eye view-number"></i>{{ $video->view_count }}
                    </span>

                    <span class="sharing-group comment-click" comment-video="{{ $video->id }}">
                    <i class="fa fa-comments"></i>
                    </span>

                    <div class="social-share-group">
                        <span class="sharing-group social-share">
                        <i class="fab fa-facebook-f"></i>
                        <span>Facebook</span>
                        </span>

                        <span class="sharing-group social-share twitter">
                        <i class="fab fa-twitter"></i>
                        <span>Twitter</span>
                        </span>

                        <span class="sharing-group social-share pinterest">
                        <i class="fab fa-pinterest-p"></i>
                        <span>Pinterest</span>
                        </span>
                    </div>
                </div>
            </div>

            @if (trim($video->content) != null)
                <div class="video-description">
                    {{ $video->content }}
                    <a href="javascrip:void(0)" class="more-less">Xem thêm</a>
                </div>
            @endif
            
            @if(!empty($video->tags))
            <div class="tags">
                @foreach($video->tags as $tag)
                    <a href="{{ url($tag->slug) }}" title="{{ $tag->name }}">#{{ $tag->name }}</a>
                @endforeach
            </div>
            @endif

            <div class="comment-block-item facebok-comment-id-{{ $video->id }}" style="display: none;"></div>
        </div>
    </div>
</div>