@foreach($videos as $video)
    @if ($video->filetype == 'gif')
        @include('frontend.videos._gif', ['video' => $video])
    @elseif ($video->filetype == 'webp')
        @include('frontend.videos._webp', ['video' => $video])
    @elseif ($video->youtube)
        @include('frontend.videos._youtube', ['video' => $video])
    @elseif (!$video->video && $video->featured_image)
        @include('frontend.videos._photo', ['video' => $video])
    @else
        @include('frontend.videos._video', ['video' => $video])
    @endif
@endforeach

<div class='pagination'>
    {{ $videos->links() }}
</div>