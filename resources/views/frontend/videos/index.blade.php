<div class="col-md-8 video-grid">
    @include('frontend.videos._list', ['videos' => $videos])
</div>

@if (!empty($categories))
<div class="col-md-4 menu-right">
    <h3>Danh mục</h3>
    <ul>
        @foreach ($categories as $category)
        <li><a href="{{ url($category->slug) }}">{{ $category->name }}</a></li>
        @endforeach
    </ul>
</div>
@endif