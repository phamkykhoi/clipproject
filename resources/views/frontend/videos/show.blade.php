<video class="video-item" autoplay muted playsinline
    id="video-item-play-{{ $video->id }}"
    onclick="pauseVideo(this)"
    style="max-height: 300px; max-width: 320px;">
    <source src="{{ asset('files/videos/' . $video->video) }}" type="video/mp4">
</video>
