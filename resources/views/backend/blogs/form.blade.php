<div class="box-body">
    <div class="form-group">
        {{ Form::label('status', trans('validation.attributes.backend.blogs.status'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
           {{ Form::select('status', $status, null, ['class' => 'form-control select2 status box-size']) }}
        </div><!--col-lg-3-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('name', trans('validation.attributes.backend.blogs.title'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.blogs.title')]) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('categories', trans('validation.attributes.backend.blogs.category'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
        @if(!empty($selectedCategories))
            {{ Form::select('categories[]', $blogCategories, $selectedCategories, ['class' => 'form-control tags box-size', 'multiple' => 'multiple']) }}
        @else
            {{ Form::select('categories[]', $blogCategories, null, ['class' => 'form-control tags box-size', 'multiple' => 'multiple']) }}
        @endif
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('tags', trans('validation.attributes.backend.blogs.tags'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
        @if(!empty($selectedtags))
           {{ Form::select('tags[]', $blogTags, $selectedtags, ['class' => 'form-control tags box-size', 'multiple' => 'multiple']) }}
        @else
            {{ Form::select('tags[]', $blogTags, null, ['class' => 'form-control tags box-size', 'multiple' => 'multiple']) }}
        @endif
        </div><!--col-lg-3-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('publish_datetime', trans('validation.attributes.backend.blogs.publish'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            @if(!empty($blog->publish_datetime))
                {{ Form::text('publish_datetime', \Carbon\Carbon::parse($blog->publish_datetime)->format('m-d-Y h:i a'), ['class' => 'form-control datetimepicker1 box-size', 'placeholder' => trans('validation.attributes.backend.blogs.publish'), 'id' => 'datetimepicker1']) }}
            @else
                {{ Form::text('publish_datetime', null, ['class' => 'form-control datetimepicker1 box-size', 'placeholder' => trans('validation.attributes.backend.blogs.publish'), 'required' => 'required', 'id' => 'datetimepicker1']) }}
            @endif
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('youtube', trans('validation.attributes.backend.blogs.youtube'), ['class' => 'col-lg-2 control-label']) }}

        <div class="col-lg-10">
            {{ Form::text('youtube', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.blogs.youtube')]) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('name', trans('validation.attributes.backend.blogs.content'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::textarea('content', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.blogs.content')]) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('name', trans('validation.attributes.backend.blogs.meta_keyword'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::text('meta_keywords', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.blogs.meta_keyword')]) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('linkvideo', 'Link video', ['class' => 'col-lg-2 control-label']) }}

        <div class="col-lg-10">
            {{ Form::text('linkvideo', null, ['class' => 'form-control box-size', 'id' => 'linkvideo', 'placeholder' => '']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('name', trans('validation.attributes.backend.blogs.video_or_gift'), ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-8">
            <div class="custom-file-input">
                <div id="upload-button" style="display: inline;">
                    <input type="file" name="video" id="video" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                    <label for="video_of_gift">
                    <i class="fa fa-upload"></i>
                    <span>Chọn video</span></label>
                </div>
            </div>

            @if (!empty($blog->video))
     
            <video id="main-video" controls style='margin-top: 5px; width: 100%; max-width: 730px; max-height: 500px; background: #000'>
                <source src="{{ asset('files/videos/' . $blog->video) }}" type="video/mp4">
            </video>
           
            @else
            <video id="main-video" controls style='display: none; margin-top: 5px; width: 100%; max-width: 730px; max-height: 500px; background: #000'>
                <source src="" type="video/mp4">
            </video>
            @endif

            <canvas id="video-canvas" style="display: none;"></canvas>

            <div id="thumbnail-container">
                Giây số <select id="set-video-seconds"></select>
                <a id="get-thumbnail" href="javascript:void(0)">Chọn ảnh từ video</a> | 
                <a href="javascript:void(0)">Chọn ảnh từ máy tính
                    <input type="file" id='upload-local' />
                </a>
            </div>

            @if (!empty($blog->featured_image))
            <div id='thumbnail-image'>
                <img src="{{ asset('files/background/' . $blog->featured_image) }}" id="thumbnail" width="450" />
                <input type="hidden" name="featured_image" id="featured_image" value="" />
            </div>
            @else
            <div id='thumbnail-image' style='display: none;'>
                <img src="" id="thumbnail" width="450" />
                <input type="hidden" name="featured_image" id="featured_image" value="" />
            </div>
            @endif
            
            <input type="hidden" name="sharing_image" id="sharing_image" value="" />
        </div>
    </div>
</div>

@section("after-scripts")
    <style>
        #thumbnail-image {
            margin-top: 10px;
            border: 1px dashed #ccc;
            padding: 3px;
            width: 420px;
        }
        
        #thumbnail-image img {
            width: 100%;
        }

        #set-video-seconds {
            padding: 5px;
        }

        input[type='file'] {
            display: inline;
        }

        #thumbnail-container a {
            padding-left: 5px;
            padding-right: 5px;
        }

        .link-video {
            border-color: #ddd;
            border-radius: 0;
            -moz-border-radius: 0;
            -webkit-border-radius: 0;
            -ms-border-radius: 0;
            box-shadow: none;
            -moz-box-shadow: none;
            -webkit-box-shadow: none;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            width: 550px;
        }
    </style>
    <script type="text/javascript">
        Backend.Blog.selectors.GenerateSlugUrl = "{{route('admin.generate.slug')}}";
        Backend.Blog.selectors.SlugUrl = "{{url('/')}}";
        Backend.Blog.init('{{ config('locale.languages.' . app()->getLocale())[1] }}');

        $(function() {
            $('#linkvideo').change(function() {
                if ($(this).val() != '') {
                    $('#main-video source').attr('src', $(this).val());
                    $('#main-video').show();
                    $("#main-video")[0].load();
                } else {
                    $('#main-video').hide();
                }
            })
        })
    </script>
@endsection