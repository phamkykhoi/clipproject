<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', 'FrontendController@index')->name('index');

Route::post('/get/states', 'FrontendController@getStates')->name('get.states');
Route::post('/get/cities', 'FrontendController@getCities')->name('get.cities');
Route::get('image', function() {
    $image = 'http://video.dev.com/files/videos/1559359223200.webp';
    $im = imagecreatefromwebp($image);
    imagejpeg($im, 'example.png');
    // imagedestroy($im);
});

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        /*
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');

        /*
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');

        /*
         * User Profile Picture
         */
        Route::patch('profile-picture/update', 'ProfileController@updateProfilePicture')->name('profile-picture.update');
    });
});


Route::get('{slug}', 'FrontendController@showPage')
    ->name('pages.show');

Route::get('/upvote/{videoId}', 'FrontendController@vote')
    ->name('pages.show');

Route::get('video/ajax', 'VideoController@index');
Route::get('video/{slug}', 'VideoController@show');
Route::post('video/delete', 'VideoController@deleteAllVideo');
Route::get('comment/{videoId}', 'CommentController@index');