<?php

/*
 * Blogs Management
 */
Route::group(['namespace' => 'Blogs'], function () {
   Route::resource('blogs', 'BlogsController', ['except' => ['show']]);
   
   // Route::post('blogs/uploadvideo', 'BlogsController@uploadVideoFromLink')
   //    ->name('blogs.post.uploadvideo');

   //For DataTables
   Route::post('blogs/get', 'BlogsTableController')
      ->name('blogs.get');

   
});
