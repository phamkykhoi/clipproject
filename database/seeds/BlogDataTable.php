<?php

use Illuminate\Database\Seeder;
use App\Models\Blogs\Blog;

class BlogDataTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Blog::class, 50)->create();
    }
}
