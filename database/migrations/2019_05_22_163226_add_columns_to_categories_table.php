<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('blog_tags', function (Blueprint $table) {
        //     $table->text('keywords');
        //     $table->text('description');
        // });

        Schema::table('blog_categories', function (Blueprint $table) {
           $table->text('keywords');
           $table->text('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('blog_tags', function (Blueprint $table) {
        //     $table->dropColumn(['keywords']);
        //     $table->dropColumn(['description']);
        // });

        Schema::table('blog_categories', function (Blueprint $table) {
            $table->dropColumn(['keywords']);
            $table->dropColumn(['description']);
        });
    }
}
